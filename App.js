import React, { useState, useEffect } from 'react';
import { Dimensions, StyleSheet, AsyncStorage } from 'react-native'
import { Button, Content, View, DeckSwiper, Card, CardItem, Text, Form, Item, Input, Label, Textarea, Root, Toast } from 'native-base';
import Layout from './Components/Layout';

const { width } =  Dimensions.get("window");

export default function App() {

    // setting state variables
    const [title, setTitle] = useState('');
    const [amount, setAmount] = useState('');
    const [notes, setNotes] = useState('');
    const [budget, setBudget] = useState([{
        title: 'Budget App',
        notes: '',
        amount: 0
    }]);

    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        getData();
        // Update the document title using the browser API
        if (budget.length > 1) {
            getTotal();
        }
    });


    function showToast(valid) {
        Toast.show({
            text: valid.text,
            buttonText: "Okay",
            duration: 3000,
            position: "top",
            type: valid.type
        });
    }

    async function getData() {
        try {
            const budget = JSON.parse(await AsyncStorage.getItem('budget'));
            if (budget) {
                setBudget(budget);
            }
        } catch (error) {
            // Toast.show({ type: 'danger', text: error.message })
            showToast({ type: 'danger', text: error.message })
        }
    }

    async function setData(budget) {
        try {
            await AsyncStorage.setItem('budget', JSON.stringify(budget));
        } catch (error) {
            showToast({ type: 'danger', text: error.message })
        }
    }
    const getTotal = () => {
        const amount = budget.map((item)=> item.amount).reduce((total, num) => total + num);

        budget.push({ title: 'total', notes: '', amount });

        setBudget(budget);
    }

    function validateForm(title, amount) {
        if(title) {
            if(amount) {
                return { type: 'success', text: 'budget updated' };
            } else {
                return { type: 'danger', text: 'amount is required' };
            }
        } else {
            return { type: 'danger', text: 'title is required' };
        }
    }

    function OnButtonPress() {
        const valid = validateForm(title, amount);

        if (valid.type === 'success') {
            budget.pop();
            budget.push({ title, notes, amount: Number(amount) });

            setTitle('');
            setAmount('');
            setNotes('');
            setBudget(budget)
            getTotal();
            setData(budget);
        }
        showToast(valid);
    }

    return (
        <Root>
            <Layout>
                <Content>
                    <Form>
                        <Item floatingLabel>
                            <Label>title</Label>
                            <Input
                                onChangeText={(title) => setTitle(title)}
                                value={title}
                            />
                        </Item>
                        <Item floatingLabel>
                            <Label>amount</Label>
                            <Input
                                onChangeText={(amount) => setAmount(parseFloat(amount))}
                                value={amount.toString()}
                            />
                        </Item>
                        <Item stackedLabel>
                            <Label>notes</Label>
                            <Textarea
                                style={ styles.textArea }
                                value={notes}
                                onChangeText={(notes) => setNotes(notes)}
                                rowSpan={5}
                            />
                        </Item>

                    </Form>
                </Content>
                <View style={styles.cardContainer}>
                    <DeckSwiper
                        // ref={(card) => this._deckSwiper = card}
                        dataSource={budget}
                        renderItem={item =>
                            <Card style={{ elevation: 3 }}>
                                <CardItem cardBody>
                                    <View style={styles.cardStyle}>

                                        {
                                            (item.title === 'Budget Bulder') ? <Text> Your Personal Budget Tracker </Text> :
                                                (item.title !== 'total') ?
                                                    <React.Fragment>
                                                        <Text>The item {item.title} has an amount of {item.amount} </Text>
                                                        <Text></Text>
                                                        <Text>notes: {item.notes}</Text>
                                                        <Text></Text>
                                                    </React.Fragment> :
                                                    <Text> the total amount due is {item.amount}</Text>
                                        }

                                    </View>
                                </CardItem>
                            </Card>
                        }
                    />
                    <Button
                        dark
                        onPress={() => OnButtonPress(this)}
                        style={styles.placeAtBottom}>
                        <Text>Submit Budget Item</Text>
                    </Button>
                </View>
            </Layout>
        </Root>

    )
}


const styles = StyleSheet.create({
    placeAtBottom: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    cardStyle : {
        height: 150,
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#ADD9FE'
    },
    cardContainer: {
        height: 250,
        backgroundColor: '#DDF9D9'
    },
    textArea: {
        flex: 1,
        width
    }
});
